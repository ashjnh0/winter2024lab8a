import java.util.Scanner;

public class TicTacToe {
	
	public static void main(String[] args) {
	
		System.out.println("Welcome to the tic-tac-toe game!!");
		
		//create board object
		Board board = new Board();
		
		boolean gameOver = false;
		
		//represents players turn, player 2 is O
		int player = 1;
		Tile playerToken = Tile.X;
		
		Scanner scanner = new Scanner(System.in);
		
		//game loop: allows players to play their turn, checks if there's a winner or if board is full
		while(!gameOver) {
			
			//print board object
			System.out.println(board);
			
			boolean validInput = false;
			do {
				//represents where to place the token
				System.out.println("Player " + player + "'s turn");
				System.out.println("Enter a number (0-2) to set the col");
				int col = scanner.nextInt();
				System.out.println("Enter a number (0-2) to set the row");
				int row = scanner.nextInt();
				validInput = board.placeToken(row, col, playerToken);
				if(!validInput) {
					System.out.println("Token cant be placed there!");
				}
			} while (!validInput);
			
			//check players turn
			if(player == 1) {
				playerToken = Tile.X;
			} else {
				playerToken = Tile.O;
			}
			
			//checks for winner/tie
			if(board.checkIfWinning(playerToken)) {
				System.out.println("\nPlayer " + player + " wins!");
				gameOver = true;
			} else if (board.checkFull()) {
				System.out.println("Board is full! its a tie");
				gameOver = true;
			} else {
				//switch players;
				if(player == 1) {
					player = 2;
				} else {
					player = 1;
				}
				
				//switch player tokens between x an o
				if(playerToken == Tile.X) {
					playerToken = Tile.O;
				} else {
					playerToken = Tile.X;
				}
			}

		}
		System.out.println(board);
		System.out.println("Good game!");
		scanner.close();
	}	

}	
