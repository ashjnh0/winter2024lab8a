enum Tile {
	X("X"),
	O("O"),
	BLANK("_");
	
	private final String name;
	
	Tile(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}

/*using toString to print values: 
enum Tile {
	BLANK, X, O;
}

enum tileToString {
	BLANK {
		@Override
		public String toString() {
			return "_";
		}
	},
	
	X {
		@Override
		public String toString() {
			return "x";
		}	
	},
	
	O {
		@Override
		public String toString() {
			return "o";
		}
	};
}*/