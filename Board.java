public class Board {
	
	private Tile[][] grid;
	
	//constructor to initialize 3x3 arr
	public Board() {
		grid = new Tile[3][3];
		
		for(int i = 0; i < grid.length; i++) {
			for(int j = 0; j < grid[i].length; j++) {
				grid[i][j] = Tile.BLANK;
			}
		}
	}
	
	//returns a string representation of the board
	@Override
	public String toString() {
		StringBuilder printBoard = new StringBuilder();
		
		for(int x = 0; x < grid.length; x++) {
			for( int s = 0; s < grid[x].length; s++) {
				printBoard.append(grid[x][s].getName()).append(" ");
			}
			printBoard.append("\n");
		}
		return printBoard.toString();
	}
	
	//checks if row and col is valid
	public boolean placeToken(int row, int col, Tile playerToken) {
		
		if(row < 0 || row > 3 || col < 0 || col > 4) {
			return false;
		} 
		
		if(grid[row][col] == Tile.BLANK) {
			grid[row][col] = playerToken;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean checkFull() {
		for(int i = 0; i < grid.length; i++) {
			for(int j = 0; j < grid[i].length; j++) {
				if (grid[i][j] == Tile.BLANK) {
					return false;
				}
			}
		}
		return true;
	}
	
	//check if tiles in a row = playerToken
	private boolean checkIfWinningHorizontal(Tile playerToken) {
		for(int row = 0; row < grid.length; row++) {
			boolean win = true;
			for(int col = 0; col < grid[row].length; col++) {
				if(grid[row][col] != playerToken) {
					win = false;
					break;
				}
			}
			if (win) {
				return true;
			}
		}
		return false;
	}
	
	//check if tiles in a col = playerToken
	private boolean checkIfWinningVertical(Tile playerToken) {
		for(int col = 0; col < grid[0].length; col++) {
			boolean win = true;
			for(int row = 0; row < grid.length; row++) {
				if(grid[col][row] != playerToken) {
					win = false;
					break;
				}
			}
			if (win) {
				return true;
			}
		}
		return false;
	}
	
	//check if tiles in diagonals = playerToken
	private boolean checkIfWinningDiagonal(Tile playerToken) {
		boolean winDiagonal = true;
		boolean winReverseDiagonal = true;
		
		for(int i = 0; i < grid.length; i++) {
			if(grid[i][i] != playerToken) {
				winDiagonal = false;
			}
		}
		
		for(int i = 0; i <grid.length; i++) {
			if(grid[i][grid.length - 1 - i] != playerToken) {
				winReverseDiagonal = false;
			}
		}
		
		return winDiagonal || winReverseDiagonal;
	}
	
	public boolean checkIfWinning(Tile playerToken) {
		return checkIfWinningHorizontal(playerToken) ||
				checkIfWinningVertical(playerToken) ||
				checkIfWinningDiagonal(playerToken);
	}
}